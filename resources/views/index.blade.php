<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="FMS für Emergency 4 Bieberfelde Modifikation">
    <meta name="author" content="">

    <title>FMS</title>


    <link rel="stylesheet" href="//emergencyx.de/css/bootstrap.min.css">
    <link rel="stylesheet" href="//emergencyx.de/css/app.css">

    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNav"
                aria-controls="collapseNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="http://www.emergencyx.de">Emergency Explorer</a>

        <div class="collapse navbar-collapse" id="collapseNav">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 text-danger">
                <li class="nav-item">
                    <a class="nav-link" href="http://www.emergencyx.de/multiplayer/browser/emergency-4">Multiplayer</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="//fms.emergencyx.de">FMS</a>
                </li>
            </ul>

        </div>
    </div>
</nav>

<div class="container" ng-app>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Join Session</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/get']) !!}
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="SessionID" name="id" type="text" autofocus
                                   required>
                        </div>

                        <button type="submit" class="btn btn-lg btn-success btn-block">View</button>
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


</div>

<script src="./bower_components/angular/angular.min.js"></script>
<script src="//emergencyx.de/bootstrap.js"></script>
</body>

</html>
