# Emergency 4 FMS für die Bieberfelde Modifikation

## Installation
- `docker-compose up -d`
- `docker-compose exec php-fpm /bin/sh`
- `cp .env.example .env` 
- `.env` bearbeiten (DB Zugang einrichten)
- `php artisan key:generate` neuen App-Key erstellen
- `php artisan migrate`