<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Session extends Model
{
    protected $guarded = [];

    public function objects(): HasMany
    {
        return $this->hasMany(FmsObject::class);
    }

    public function getStyle(): string
    {
        if ($this->style === '') {
            return '/app.css';
        }

        return $this->style;
    }
}
