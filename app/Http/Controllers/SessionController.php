<?php

namespace App\Http\Controllers;

use App\Models\CategoryList;
use App\Models\SessionManager;
use App\Session;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SessionController extends Controller
{
    protected $sessionManager;

    public function __construct(SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    public function index()
    {
        return view('index');
    }

    public function get(Request $request)
    {
        $uuid = $request->get('id');
        if (is_null($uuid)) {
            throw new NotFoundHttpException('Session not found');
        }

        $session = $this->sessionManager->read($uuid);

        return redirect('/show/' . $session->uuid);
    }

    public function show(Request $request, $id)
    {
        $session = $this->sessionManager->read($id);

        if (is_null($session)) {
            return redirect('/');
        }

        $categories = new CategoryList($session);

        if ($request->isXmlHttpRequest()) {
            return $session->objects;
        } else {
            return view('show', compact('categories', 'session'));
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $name = $request->get('name');
        $objects = $request->get('objects', []);
        $style = $request->get('style', '/app.css');

        $session = $this->sessionManager->create($name, $objects, $style);

        if ($request->isJson()) {
            if (is_null($session)) {
                return [
                    'status' => 'FAILURE',
                    'message' => 'Something went wrong',
                ];
            }

            return [
                'status' => 'OK',
                'sessionId' => $session->uuid,
            ];
        }

        if (is_null($session)) {
            return redirect('/');
        }

        return redirect('/show/' . $session->uuid);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function update(Request $request)
    {
        $sessionId = $request->get('sessionId');
        $session = $this->sessionManager->read($sessionId);


        $this->sessionManager->update(
            $session,
            collect($request->get('objects', []))
        );

        return [
            'status' => 'OK',
            'sessionId' => $sessionId,
        ];
    }

}
