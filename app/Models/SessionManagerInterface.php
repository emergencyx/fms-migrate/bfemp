<?php

namespace App\Models;

use App\Session;
use Illuminate\Support\Collection;

interface SessionManagerInterface
{
    public function create($name, $objects): Session;

    public function read($sessionId);

    public function update(Session $session, Collection $objects);
}

