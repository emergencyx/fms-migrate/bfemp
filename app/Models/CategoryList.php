<?php

namespace App\Models;

use App\Session;

class CategoryList
{
    public function __construct(Session $session)
    {
        $categories = [];

        if (!$session->relationLoaded('objects')) {
            $session->load('objects');
        }

        foreach ($session->objects as $object) {
            $categories[$object['category']]['name'] = $object['category'];
            $categories[$object['category']]['objects'][] = $object;
        }

        $this->categories = $categories;
    }

    public function get()
    {
        return $this->categories;
    }
}
