<?php

namespace App\Models;

use App\Broadcast\Broadcaster;
use App\FmsObject;
use App\Session;
use Illuminate\Support\Collection;

class SessionManager implements SessionManagerInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Broadcaster
     */
    protected $broadcaster;

    /**
     * SessionManager constructor.
     * @param Session $session
     * @param Broadcaster $broadcaster
     */
    public function __construct(Session $session, Broadcaster $broadcaster)
    {
        $this->session = $session;
        $this->broadcaster = $broadcaster;
    }

    /**
     * @param $name
     * @param $objects
     * @param string $style
     * @return Session
     * @throws \Throwable
     */
    public function create($name, $objects, $style = '/app.css'): Session
    {
        $uuid = $this->generateUUID();

        $session = $this->session->create([
            'uuid' => $uuid,
            'name' => $name,
            'style' => $style,
        ]);

        $sessionObjects = [];
        foreach ($objects as $category => $gameObjects) {
            foreach ($gameObjects as $gameObject) {
                $sessionObjects[] = new FmsObject([
                    'log' => $gameObject['log'],
                    'name' => $gameObject['display'],
                    'status' => $gameObject['state'],
                    'category' => $category,
                ]);
            }
        }

        $session->objects()->saveMany($sessionObjects);
        return $session;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function generateUUID()
    {
        $data = random_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public function read($sessionId)
    {
        return $this->session->newQuery()
            ->where('uuid', $sessionId)
            ->with('objects')
            ->firstOrFail();
    }

    public function update(Session $session, Collection $objects)
    {
        $storedObjects = $session->objects()->whereIn('log', $objects->pluck('name'))->limit(50)->get();

        $updated = [];

        foreach ($objects as $object) {
            $gameObject = $storedObjects->where('log', $object['name'])->first();

            if (is_null($gameObject)) {
                continue;
            }

            // there is no status 0 - its a C!
            if ($object['status'] == '0' && !starts_with($object['name'], 'BMA')) {
                $object['status'] = 'C';
            }

            // status 9 means patrol for police which
            // means free on radio
            if ($object['status'] == '9') {
                $object['status'] = '1';
            }

            $gameObject->status = $object['status'];
            $gameObject->save();

            $updated[] = $gameObject->toArray();
        }

        $this->broadcaster->publish($session->uuid, ['updated' => $updated]);
    }
}

