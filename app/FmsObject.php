<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FmsObject extends Model
{
    public $timestamps = false;

    protected $fillable = ['log', 'name', 'status', 'category'];

    protected $hidden = ['session_id'];

    public function session()
    {
        return $this->belongsTo(Session::class);
    }
}
