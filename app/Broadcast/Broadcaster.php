<?php

namespace App\Broadcast;


use GuzzleHttp\Client;

class Broadcaster
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Broadcaster constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function publish(string $channel, array $body)
    {
        try {
            $response = $this->client->post('http://localhost/pub/' . $channel, ['timeout' => 2, 'json' => $body]);
            logger()->debug('broadcast ok - ' . $response->getStatusCode());
        } catch (\Exception $e) {
            logger()->error('broadcast failed - ' . $e->getMessage());
        }
    }
}