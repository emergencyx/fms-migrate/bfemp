<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration
{
    public function up()
    {
        Schema::create('sessions', function($table) {
            $table->increments('id')->unsigned();
            $table->string('uuid')->index();
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        if (Schema::hasTable('sessions')) {
            Schema::drop('sessions');
        }
    }
}
