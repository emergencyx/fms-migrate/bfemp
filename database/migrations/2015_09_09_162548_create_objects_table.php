<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    public function up()
    {
        Schema::create('fms_objects', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->string('name');
            $table->string('status');
            $table->string('category');
            $table->string('log')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('fms_objects')) {
            Schema::drop('fms_objects');
        }
    }
}
